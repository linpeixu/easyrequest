package com.cloudling.request.type;

/**
 * 描述: 请求体类型
 * 作者: lpx
 * 日期: 2022/2/14
 */
public enum RequestBodyType {
    /**
     * 默认
     */
    DEFAULT,
    /**
     * JSON
     */
    JSON,
    /**
     * 表单
     */
    FORM
}
